require('dotenv').config()
import { Client, Network } from '../src/'

describe('Library Test', () => {

    it('should be able to instantiate Client', () => {
        const net = Network.MAIN
        const phrase = "unfair panel dress elite acid brush broken drink two camp cinnamon dance"
        const btcClient = new Client(net, phrase)
        expect(btcClient).toBeInstanceOf(Client)
        const network = btcClient.getNetwork(net)
        expect(network.bech32).toEqual('bc')
    })

})
